//
//  TodayViewController.swift
//  UmwerkToday
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit
import NotificationCenter

class TodayViewController: UIViewController, NCWidgetProviding,UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var userArray:NSMutableArray = []
    let api = Api()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "UserCell", bundle:nil)
        self.tableView.register(nibName,forCellReuseIdentifier: "UserCell")
        self.extensionContext?.widgetLargestAvailableDisplayMode = .expanded;
        
        retrieveUsers()
        // Do any additional setup after loading the view from its nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
    func widgetActiveDisplayModeDidChange(_ activeDisplayMode: NCWidgetDisplayMode, withMaximumSize maxSize: CGSize) {
        if activeDisplayMode == .expanded {
            self.preferredContentSize = CGSize(width: CGFloat(0.0), height: CGFloat(210.0))
        }
        else if activeDisplayMode == .compact {
            self.preferredContentSize = maxSize
        }
        
    }
    // MARK: - tableviewdatasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.userArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    // MARK: - tableviewdelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell: UserCell? = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell?
        let user = self.userArray[indexPath.row] as? User
        cell?.setUser(user: user!)
        return cell!
    }
    // MARK: - Api call
    func retrieveUsers() {
        let r1 = Int(0 + arc4random_uniform(3 - 0)+1)
        let r2 = Int(4 + arc4random_uniform(7 - 4 )+1)
        let r3 = Int(8 + arc4random_uniform(9 - 8 )+1)
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        var i = 0
        api.getItems(attr: r2, completion: { (array, error) in
            if error.characters.count == 0{
                for item in array{
                    i+=1
                    let user:NSDictionary = item as! NSDictionary
                    let url = user["url"]
                    if i == r1 || i == r2 || i == r3{
                        self.api.getUser(url:url as! String , completion: { (userDetailed, error) in
                            if(error.characters.count == 0){
                                let u = User()
                                self.userArray.add(u.initWithDict(dict: userDetailed))
                                if self.userArray.count == 3{
                                    OperationQueue.main.addOperation {() -> Void in
                                        self.indicator.isHidden = true
                                        self.indicator.stopAnimating()
                                        self.tableView.reloadData()
                                    }
                                }
                            }else{
                                OperationQueue.main.addOperation {() -> Void in
                                    self.indicator.isHidden = true
                                    self.indicator.stopAnimating()
                                }
                            }
                        })
                    }
                }
            }
        })
    }
    
    
}
