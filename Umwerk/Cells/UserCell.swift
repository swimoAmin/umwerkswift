//
//  UserCell.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit
import ImageLoader

class UserCell: UITableViewCell {
    var user: User!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var registerDateLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func setUser(user:User){
        self.user = user
        self.usernameLabel.text = user.username
        self.avatar.layer.cornerRadius = 27
        self.avatar.clipsToBounds = true
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        formatter.locale = NSLocale.current
        let creatdDate: Date? = formatter.date(from: user.created)
        formatter.dateFormat = "dd-MM-yyyy"
        let registerDate: String = formatter.string(from: creatdDate!)
        self.registerDateLabel.text = "Member since: \(registerDate)"
        self.avatar.load.request(with: user.image)
    }
    
}
