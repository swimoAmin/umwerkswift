//
//  UserDetailleController.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit
import ImageLoader

class UserDetailleController: UIViewController {

    @IBOutlet weak var userImage: UIImageView!
    var backgroundColor: UIColor!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var followerLabel: UILabel!
    @IBOutlet weak var githubButton: UIButton!
    @IBOutlet weak var blogButton: UIButton!
    @IBOutlet weak var leading: NSLayoutConstraint!
    @IBOutlet weak var trailing: NSLayoutConstraint!
    var user: User!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundColor = AppDelegate.instance().backgroundColor
        self.view.backgroundColor = self.backgroundColor
        self.navigationController?.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.tintColor = self.backgroundColor
        self.navigationItem.title = self.user?.username
        self.fillUserDetails()

    }
    
    func fillUserDetails() {
        if UI_USER_INTERFACE_IDIOM() == .pad {
            self.leading.priority = 750
            self.trailing.priority = 250
            self.userImage.contentMode = .scaleAspectFill
        }
        self.addBorder(self.githubButton)
        self.addBorder(self.blogButton)
        self.userNameLabel.text = self.user?.username
        self.addressLabel.text = self.user?.location
        self.emailLabel.text = self.user?.email
        self.followerLabel.text = "\(self.user.followers) Followers"
        self.blogButton.isHidden =  !(self.user?.blog.hasPrefix("www"))! && !(self.user?.blog.hasPrefix("http://"))!

        self.userImage.load.request(with: user.image)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func addBorder(_ button: UIButton) {
        button.layer.cornerRadius = 4
        button.layer.borderWidth = 2
        button.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func blog(_ sender: Any) {
        let application = UIApplication.shared
        application.open(URL(string: (self.user?.blog)!)!, options: [:], completionHandler: nil)
    }
    
    @IBAction func github(_ sender: Any) {
        let application = UIApplication.shared
        application.open(URL(string: (self.user?.html_url)!)!, options: [:], completionHandler: nil)
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
