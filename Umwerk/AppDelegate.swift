//
//  AppDelegate.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var backgroundColor:UIColor!
    class func instance() -> AppDelegate {
        return (UIApplication.shared.delegate! as? AppDelegate)!
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        #if UMWERKV2
            self.backgroundColor = UIColor.black
        #else
            self.backgroundColor = UIColor(red: CGFloat(46 / 255.0), green: CGFloat(171 / 255.0), blue: CGFloat(240 / 255.0), alpha: CGFloat(1.0))
        #endif
        UINavigationBar.appearance().barTintColor = UIColor.white
        UINavigationBar.appearance().isTranslucent = false
        UIBarButtonItem.appearance().tintColor = self.backgroundColor
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName: self.backgroundColor]
        if UI_USER_INTERFACE_IDIOM() == .pad {
            UILabel.appearance().font = UIFont.systemFont(ofSize: CGFloat(20))
        }

        // Override point for customization after application launch.
        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    
    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    
}

