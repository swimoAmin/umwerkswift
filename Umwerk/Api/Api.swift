//
//  Api.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit
import Alamofire

class Api: NSObject {
    let USER_URL = "https://api.github.com/search/users?q=language%%3Ajava&page=%lu&per_page=10"
    
    
    func getItems(attr: Int, completion: @escaping (_ dict: NSArray, _ errorMessage: String) -> Void) {
        
        Alamofire.request(String(format: USER_URL, UInt(attr))).responseJSON { response in
            do {
                 let json = try  JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                
                
                if  let error = json["message"] {
                     completion([],error as! String)
                }else{
                    let items = json["items"] as! NSArray
                    if items.count == 0{
                        completion(items,"No user Available")
                    }else{
                        completion(items,"")
                    }
                }
                
            } catch {
                print("error serializing JSON: \(error)")
            }
        }
        
    }
    func getUser(url: String, completion: @escaping (_ dict: NSDictionary, _ errorMessage: String) -> Void) {
        
        Alamofire.request(url).responseJSON { response in
            do {
                let user = try  JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
                
                if  let error = user["message"] {
                    completion([:],error as! String)
                }else{
                    completion(user,"")
                    
                }

            } catch {
                completion([:],"Please try again")
                print("error serializing JSON: \(error)")
            }
        }
        
    }

}
