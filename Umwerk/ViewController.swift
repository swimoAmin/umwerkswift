//
//  ViewController.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var indicator: UIActivityIndicatorView!
    @IBOutlet weak var tableView: UITableView!
    var userArray:NSMutableArray = []
    var backgroundColor: UIColor!
    var page: Int = 0
    var isIPad: Bool = false
    let api = Api()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.backgroundColor = AppDelegate.instance().backgroundColor
        self.view.backgroundColor = self.backgroundColor
        self.userArray = []
        self.page = 1
        self.retrieveUsers(self.page)
        self.isIPad = UI_USER_INTERFACE_IDIOM() == .pad
        let nibName = UINib(nibName: "UserCell", bundle:nil)
        self.tableView.register(nibName,forCellReuseIdentifier: "UserCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - tableviewdatasource
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.page < 100 ? self.userArray.count + 1 : self.userArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == self.userArray.count {
            return self.isIPad ? 70 : 35
        }
        return self.isIPad ? 100 : 70
    }
    
    // MARK: - tableviewdelegate
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == self.userArray.count {
            var lastCell: UITableViewCell? = tableView.dequeueReusableCell(withIdentifier: "Cell")
            if lastCell == nil {
                lastCell = UITableViewCell(style: .default, reuseIdentifier: "Cell")
            }
            lastCell?.textLabel?.text = "Loding more ..."
            lastCell?.textLabel?.textColor = self.backgroundColor
            self.page += 1
            self.retrieveUsers(self.page)
            return lastCell!
        }
        else {
            let cell: UserCell? = tableView.dequeueReusableCell(withIdentifier: "UserCell") as! UserCell?
            let user = self.userArray[indexPath.row] as? User
            cell?.setUser(user: user!)
            return cell!
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let u: User? = self.userArray[indexPath.row] as? User
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let userDetail: UserDetailleController? = (storyboard.instantiateViewController(withIdentifier: "UserDetailleController") as? UserDetailleController)
        userDetail?.user = u
        self.navigationController?.pushViewController(userDetail!, animated: true)
    }
    
    // MARK:- Sort button
    
    @IBAction func sortList(_ sender: Any) {
        let sortedArray = userArray.sortedArray(comparator: {
            (obj1, obj2) -> ComparisonResult in
            
            let p1 = obj1 as! User
            let p2 = obj2 as! User
            let result = p1.name.compare(p2.name)
            return result
        })
        self.userArray = []
        self.userArray.setArray(sortedArray)
        self.tableView.reloadData()
    }
    // MARK:- Alert
    
    func alertError(_ errorMessage: String) {
        OperationQueue.main.addOperation {() -> Void in
            let alert = UIAlertController(title: "Error!!", message: errorMessage, preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Try again", style: .default, handler: {(_ action: UIAlertAction) -> Void in
                self.retrieveUsers(self.page)
            })
            let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: {(_ action: UIAlertAction) -> Void in
            })
            alert.addAction(okAction)
            alert.addAction(cancelAction)
            self.present(alert, animated: true, completion: { _ in })
            self.indicator.isHidden = true
            self.indicator.stopAnimating()
            self.tableView.reloadData()
        }
    }
    
    // MARK: - API CALL
    
    func retrieveUsers(_ page: Int) {
        self.indicator.startAnimating()
        self.indicator.isHidden = false
        api.getItems(attr: page, completion: { (array, error) in
            if error.characters.count == 0{
                for item in array{
                    let user:NSDictionary = item as! NSDictionary
                    let url = user["url"]
                    self.api.getUser(url:url as! String , completion: { (userDetailed, error) in
                        if(error.characters.count == 0){
                            let u = User()
                            self.userArray.add(u.initWithDict(dict: userDetailed))
                            if self.userArray.count == page * 10{
                                OperationQueue.main.addOperation {() -> Void in
                                    self.indicator.isHidden = true
                                    self.indicator.stopAnimating()
                                    self.tableView.reloadData()
                                }
                            }
                        }else{
                            OperationQueue.main.addOperation {() -> Void in
                                self.indicator.isHidden = true
                                self.indicator.stopAnimating()
                                self.alertError(error)
                            }
                        }
                    })
                }
            }else{
                self.alertError(error)
            }
        })
    }
    
    
}

