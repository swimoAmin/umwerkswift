//
//  User.swift
//  Umwerk
//
//  Created by mohamed souiden on 2017. 01. 15..
//  Copyright © 2017. mohamed souiden. All rights reserved.
//

import UIKit

class User: NSObject {
    var username: String = ""
    var image: String = ""
    var name: String = ""
    var html_url: String = ""
    var company: String = ""
    var blog: String = ""
    var location: String = ""
    var email: String = ""
    var created: String = ""
    var updated: String = ""
    var userId: Int = 0
    var followers: Int = 0
    var following: Int = 0
    
    func initWithDict(dict: NSDictionary) -> User{
        
        self.username = (dict["login"] is NSNull) ? "N/A" : (dict["login"] as? String)!
        self.image = (dict["avatar_url"] is NSNull) ? "N/A" : (dict["avatar_url"] as? String)!
        self.userId = (dict["id"] as! Int)
        self.name = (dict["name"]  is NSNull) ? "N/A" : (dict["name"] as? String)!
        self.html_url = (dict["html_url"]  is NSNull) ? "N/A" : (dict["html_url"] as? String)!
        self.company = (dict["company"]  is NSNull) ? "N/A" : (dict["company"] as? String)!
        self.blog = (dict["blog"] is NSNull) ? "N/A" : (dict["blog"] as? String)!
        self.location = (dict["location"] is NSNull) ? "N/A" : (dict["location"] as? String)!
        self.email = (dict["email"]  is NSNull) ? "N/A" : (dict["email"] as? String)!
        self.created = (dict["created_at"] as? String)!
        self.updated = (dict["updated_at"] as? String)!
        self.followers = (dict["followers"] as! Int)
        self.following = (dict["following"] as! Int)
        return self
        
    }
    
    
}
